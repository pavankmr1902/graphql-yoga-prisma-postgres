import { makeExecutableSchema } from "@graphql-tools/schema";
import type { User } from "@prisma/client";
import type { GraphQLContext } from "./src/context";

const typeDefinitions = /* GraphQL */ `
  # type User {
  #   id: ID!
  #   name: String
  # }

  type Query {
    info: String!
    feed: [User!]!
  }

  type Mutation {
    createUser(name: String!, id: Int!): User!
  }
`;

const resolvers = {
  Query: {
    info: () => `This is the API of a Hackernews Clone`,
    feed: (parent: unknown, args: {}, context: GraphQLContext) =>
      context.prisma.user.findMany(),
  },
  User: {
    id: (parent: User) => parent.id,
    name: (parent: User) => parent.name,
  },
  Mutation: {
    createUser: (
      parent: unknown,
      args: {
        name: string;
        id: number;
      },
      context: GraphQLContext
    ) =>
      context.prisma.user.create({
        data: {
          name: args.name,
          id: args.id,
        },
      }),
  },
};

export const schema = makeExecutableSchema({
  resolvers: [resolvers],
  typeDefs: [typeDefinitions],
});
